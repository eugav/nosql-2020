package ru.mai.nosql.server

import com.mongodb.client.MongoClients
import org.apache.http.HttpHost
import org.elasticsearch.client.RestClient
import org.elasticsearch.client.RestHighLevelClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.MongoTemplate


@SpringBootApplication
class DemoApplication {
    @Bean
    fun mongoOperations(
            @Value("${'$'}{mongo.url}") connectionString: String,
            @Value("${'$'}{mongo.database}") databaseName: String
    ): MongoOperations {
        return MongoTemplate(MongoClients.create(connectionString), databaseName)
    }

    @Bean
    fun elasticRestClient(): RestHighLevelClient {
        return RestHighLevelClient(
                RestClient.builder(
                HttpHost("localhost", 9200, "http")
        ))
    }
}

fun main(args: Array<String>) {
    runApplication<DemoApplication>(*args)
}