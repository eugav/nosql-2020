package ru.mai.nosql.server.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

data class Student(
        var id: String?,
        var name: String?,
        var age: Int?,
        var addresses: List<Address>?
)

data class Address(
        var city: String,
        var street: String
)

@Document("student")
data class PersistentStudent(
        @Id
        var id: String?,
        var name: String?,
        var age: Int?,
        var addresses: List<PersistentAddress>? = null
)

data class PersistentAddress(
        var city: String,
        var street: String
)