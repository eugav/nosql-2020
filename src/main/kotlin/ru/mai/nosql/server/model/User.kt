package ru.mai.nosql.server.model

data class User(
        val id: String,
        val displayName: String,
        val reputation: Int
)