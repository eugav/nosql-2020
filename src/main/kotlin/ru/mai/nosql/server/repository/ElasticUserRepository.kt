package ru.mai.nosql.server.repository

import org.elasticsearch.action.ActionListener
import org.elasticsearch.action.get.GetRequest
import org.elasticsearch.action.get.GetResponse
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestHighLevelClient
import org.springframework.stereotype.Repository
import ru.mai.nosql.server.model.User
import java.lang.Exception

@Repository
class ElasticUserRepository(
        val restClient: RestHighLevelClient
) {

    companion object {
        const val INDEX_NAME = "stackoverlow"
    }

    fun findById(userId: String): User? {
        val request = GetRequest(INDEX_NAME, userId)
        val response = restClient.get(request, RequestOptions.DEFAULT)
        // restClient.getAsync(request, RequestOptions.DEFAULT, OnGetResponse())
        return if (response.isExists) {
            val responseMap = response.sourceAsMap
            User(
                    id = responseMap["Id"] as String,
                    displayName = responseMap["DisplayName"] as String,
                    reputation = (responseMap["Reputation"] as String).toInt()
            )
        } else {
            null
        }
    }

//
//    private class OnGetResponse: ActionListener<GetResponse> {
//        override fun onResponse(p0: GetResponse?) {
//            TODO("Not yet implemented")
//        }
//
//        override fun onFailure(p0: Exception?) {
//            TODO("Not yet implemented")
//        }
//    }

}