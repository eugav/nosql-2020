package ru.mai.nosql.server.repository

import org.springframework.stereotype.Repository
import ru.mai.nosql.server.model.PersistentStudent

@Repository("inMemoryStudentRepository")
class InMemoryStudentRepository : StudentRepository {

    private val students: MutableMap<String, PersistentStudent> = listOf(
            PersistentStudent("1", "Иван", 21),
            PersistentStudent("2", "Алексей", 22),
            PersistentStudent("3", "Сергей", 23),
    )
            .associateBy { student -> student.id ?: "" }
            .toMutableMap()

    override fun findAll(): List<PersistentStudent> {
        return students.values.toList()
    }

    override fun addStudent(student: PersistentStudent): PersistentStudent {
        return student.copy(id = (students.size.toLong() + 1).toString()).also {
            students[it.id ?: ""] = it
        }
    }

    override fun updateStudent(student: PersistentStudent): PersistentStudent {
        if (!students.containsKey(student.id)) {
            throw IllegalArgumentException("Student with id = ${student.id} does not exist")
        }
        students[student.id ?: ""] = student
        return student
    }

    override fun deleteStudent(studentId: String) {
        students.remove(studentId)
    }
}