package ru.mai.nosql.server.repository

import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Repository
import ru.mai.nosql.server.model.PersistentStudent

@Repository("studentRepository")
class MongoStudentRepository(
        val mongoOperations: MongoOperations
) : StudentRepository {

    override fun findAll(): List<PersistentStudent> {
        return mongoOperations.findAll(PersistentStudent::class.java)
    }

    override fun addStudent(student: PersistentStudent): PersistentStudent {
        return mongoOperations.save(student)
    }

    override fun updateStudent(student: PersistentStudent): PersistentStudent {
        return mongoOperations.save(student)
    }

    override fun deleteStudent(studentId: String) {
        mongoOperations.remove(Query.query(Criteria.where("_id").`is`(studentId)), PersistentStudent::class.java)
    }

}