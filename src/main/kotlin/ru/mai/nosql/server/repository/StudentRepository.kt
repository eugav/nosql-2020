package ru.mai.nosql.server.repository

import ru.mai.nosql.server.model.PersistentStudent

interface StudentRepository {
    fun findAll(): List<PersistentStudent>
    fun addStudent(student: PersistentStudent): PersistentStudent
    fun updateStudent(student: PersistentStudent): PersistentStudent
    fun deleteStudent(studentId: String)
}