package ru.mai.nosql.server.rest

import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import ru.mai.nosql.server.model.Address
import ru.mai.nosql.server.model.PersistentAddress
import ru.mai.nosql.server.model.PersistentStudent
import ru.mai.nosql.server.model.Student
import ru.mai.nosql.server.repository.StudentRepository

@RestController
class StudentController(
        val studentRepository: StudentRepository
) {
    @GetMapping("/api/student/hello")
    fun hello(name: String?): String {
        return "Hello $name"
    }

    @GetMapping("/api/student")
    fun listStudents(): List<Student> {
        return studentRepository.findAll().map { it.toApi() }
    }

    @PostMapping("/api/student")
    fun addStudent(@RequestBody student: Student): Student {
        return studentRepository
                .addStudent(student.toPersistent())
                .toApi()
    }

    @PutMapping("/api/student/{id}")
    fun updateStudent(@PathVariable id: String, @RequestBody student: Student): Student {
        return studentRepository
                .updateStudent(student.copy(id = id).toPersistent())
                .toApi()
    }

    @DeleteMapping("/api/student/{id}")
    fun deleteStudent(@PathVariable id: String) {
        studentRepository.deleteStudent(id)
    }

    private fun Student.toPersistent(): PersistentStudent {
        return PersistentStudent(
                id = this.id,
                name = this.name,
                age = this.age,
                addresses = this.addresses?.map {
                    PersistentAddress(
                            city = it.city,
                            street = it.street
                    )
                }
        )
    }

    private fun PersistentStudent.toApi(): Student {
        return Student(
                id = this.id,
                name = this.name,
                age = this.age,
                addresses = this.addresses?.map {
                    Address(
                            city = it.city,
                            street = it.street
                    )
                }
        )
    }
}