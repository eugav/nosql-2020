package ru.mai.nosql.server.rest

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import ru.mai.nosql.server.model.User
import ru.mai.nosql.server.repository.ElasticUserRepository

@RestController
class UserController(
        val userRepository: ElasticUserRepository
) {
    @GetMapping("/api/user/{id}")
    fun getById(@PathVariable("id") id: String): User? {
        return userRepository.findById(id)
    }
}